# -*- coding: utf-8 -*-
from parse_comsol import parse_comsol_data_3D

import numpy as np
from write_dx import write_dx

from scipy import signal

## Gaussian blur
def blur(g,delta,width,mode='same'):
    """Apply a gaussian blur to a gridData grid"""
    width = float(width)
    
    assert( np.size(delta) == 3 )
    kernelSize = [int(np.ceil(width*5.0/i)) for i in delta]
    for a in kernelSize:
        assert( a > 3 )

    ## Construct gaussian convolution kernel
    i,j,k = [np.arange(int(a)) for a in kernelSize] # get indeces
    i,j,k = np.meshgrid(i,j,k,indexing='ij')   # turn indeces into meshGrid indeces
    gauss = [signal.gaussian( a+1, width/x )
             for a,x in zip(kernelSize,delta)]
    kernel = gauss[0][i]*gauss[1][j]*gauss[2][k]
    kernel = kernel/kernel.sum()

    ## Convolve to blur
    return signal.fftconvolve(g, kernel, mode=mode)
                # edges=g.edges)

mV=600
dataFile = 'potential-%03d.txt' % mV
out =      '../potential-%03d.dx'  % mV

print("Reading",dataFile)
comsol_data = parse_comsol_data_3D(dataFile)
x,y,z,fields = comsol_data

## Create 3D mesh 
dx,dy,dz = [a[1]-a[0] for a in (x,y,z)]

## Get grid dimensions
delta = np.array( (dx,dy,dz) )                       # Spacing between grid cells
origin = np.array( (np.min(x),np.min(y),np.min(z)) ) # "Lower-left" corner of grid

## Convert electric field in V to a potential in kcal_mol
potential = fields[0] * 23.06055 * mV / 600.0 # 
q_effective = -0.25             # Effective charge per nucleotide
potential = q_effective * potential # Change electric potential into an energy potential / nt
potential[ np.isnan(potential) ] = 5 # Make pipette impenetrable 

## Blur potential
sh0 = np.array( potential.shape ) # number of cells before blurring
potential = blur( potential, delta, width=1, mode='valid' ) # blur
sh1 = np.array(np.shape(potential)) # number of cells after blurring
origin = origin + delta * ((sh0-sh1)/2) # Shift origin 

## Subtract value at upper boundary so there is no force at this part of grid
potential = potential - np.mean( potential[:,:,0] )

write_dx(out, potential,
         origin = origin*10,     # convert from nm to Angstrom
         delta = delta*10,
         fmt = '%.6f' )
