# Multi-resolution DNA nanostructures

This tutorial introduces a Python3 package called `mrdna` that makes it easy to run simulations of DNA nanostructures.
The multi-resolution approach provides the benefits of coarse-grained modeling, while resulting in a high-resolution structure suitable for atomistic simulations.

## Getting started

### Required hardware and operating system

Please note that this tutorial is written for the linux platform and x86 architecture.
Proceed at your own risk using other operating systems and architectures.
Currently, a CUDA-enabled GPU is required for running simulations using the ARBD engine.

### Required software

Make sure you have the `mrdna` package properly installed.
You will also need the `jupyter` package installed to your Python environment. 
Finally, you will need to install
[VMD](https://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=VMD), 
[NAMD](https://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=NAMD)
and [ARBD](http://bionano.physics.illinois.edu/arbd).

## Instructions

Follow the pdf for steps 1-2, which cover the usage of a command line utility that makes fast multi-resolution simulations of DNA objects very easy to perform.
Then launch the Jupyter notebook "step3.ipynb" for an introduction to scripting with the `mrdna` framework.
If you have any questions or concerns, please contact Chris Maffeo at [cmaffeo2@illinois.edu](mailto:cmaffeo2@illinois.edu).
